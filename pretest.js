
//1. Mencetak Bilangan Ganjil//

function printOdds(arr)
{
  arr.forEach (function(el)
  {
    if (el % 2)
      console.log (el);
  });
}

a = [1,2,3,4,5,6,7,8,9,10];
printOdds(a);

//2. Mencetak angka genap

function counteven(num){
    var c = 0;
    for(let b = 0; b<=num; b++){
        if(b%2==0){
            c++;
        }
    }
    console.log(c);
}

//3. Mencari nilai max

function getmax(arr){
    var max = 0;
    for (let x=0; x<=arr.length; x++){
    if(arr[x] > max){
        max = arr[x]
    }
    }
console.log(max);
}

//4. Sorting nilai array

function swap(arr, first_Index, second_Index){
    var temp = arr[first_Index];
    arr[first_Index] = arr[second_Index];
    arr[second_Index] = temp;
}

function sortasc(arr){

    var len = arr.length,
        i, j, stop;

    for (i=0; i < len; i++){
        for (j=0, stop=len-i; j < stop; j++){
            if (arr[j] > arr[j+1]){
                swap(arr, j, j+1);
            }
        }
    }

    return arr;
}

//5. Print person dan age menggunakan method

class Person{
    constructor(name, date){
        this.name = name;
        this.date = date;
    }

    getName(){
        console.log('Halo saya adalah' + ' ' + this.name)
    }

    getAge(){
        var today = new Date();
        var thisyear = today.getFullYear();
        var birthday = new Date(this.date);
        var birthyear = birthday.getFullYear();
        var umur = 0;

        umur = thisyear - birthyear;

        console.log('Saya berumur' + ' ' + umur + ' ' + 'tahun');

    }

}

var user = new Person("taslina", "2000-12-31")
user.getName();
user.getAge();


